# SPDX-FileCopyrightText: 2022 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

influxdb>=5.2.2
pyyaml>=5.1
paho-mqtt>=1.4.0
