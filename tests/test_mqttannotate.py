# SPDX-FileCopyrightText: 2022 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

import unittest


class TestMqttAnnotate(unittest.TestCase):
    def test_hello(self):
        self.assertEqual("hello", "hello")
