<!--
SPDX-FileCopyrightText: 2022 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: GPL-3.0-only
-->

# Mqtt Annotate
This is a small application with that opens a window with two text fields and a send button, you put the title of the annotation in one field and the text of the annotation on the other field and then presses send. 

## Install 
This project is hosted on freemelts public APT repo and here on gitlab. Either clone the project and install it manually or configure apt to read pacakges from `apt.opensource.freemelt.com/debian`. On debian buster you can do this by adding `deb [trusted=yes] http://apt.opensource.freemelt.com/debian buster main` to `/etc/apt/sources.list` and run `sudo apt update` followed by `sudo apt install  freemelt-mqttannotate`.

## Configure Grafana

Find the dashboard settings.

![](/uploads/5e28299c70066be920cc1c2cb22bcb37/DashboardSettings.png)

Adda a new annotation.

![](/uploads/0cbc9e7815391429b9525fda6728acc2/AnnotationAdd.png)

Make the annotation look like below, note to put description in the field for text. And use the query below as a query.

`SELECT title, description from annotations WHERE $timeFilter ORDER BY time ASC`

![](/uploads/7a8eabcc622e8c3924361a1a9b32ef31/AnnotationQuery.png)

# License
Copyright © 2022 Freemelt AB <opensource@freemelt.com>

Mqtt annotate is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3 of the License.
