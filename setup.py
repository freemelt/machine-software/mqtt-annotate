# SPDX-FileCopyrightText: 2022 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

from setuptools import setup, find_packages
from mqttannotate import __version__

with open("README.md") as readme_file:
    long_description = readme_file.read()

setup(
    name="mqttannotate",
    version=__version__,
    maintainer="Freemelt AB",
    maintainer_email="opensource@freemelt.com",
    description="Control software for metal 3D printers from Freemelt AB",
    long_description=long_description,
    url="https://gitlab.com/freemelt/machine-software/mqtt-annotate",
    packages=find_packages(exclude=["tests"]),
    install_requires=[
        "paho-mqtt>=1.4.0",
    ],
    entry_points={
        'console_scripts': [
            'mqttannotate=mqttannotate.__main__:main'
        ]
    },

    python_requires=">=3.7",
    classifiers=[
        "Environment :: Console",
        "Natural Language :: English",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: Implementation :: CPython",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
