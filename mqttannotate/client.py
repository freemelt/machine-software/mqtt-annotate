# SPDX-FileCopyrightText: 2019-2021 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

# Built-in
import logging
from tkinter import *
from tkinter import ttk
from time import sleep

# PyPI
import paho.mqtt.client as mqtt

# Freemelt
from servicelib.mqtt import MQTTMessage

LOG = logging.getLogger("MQTT")


class MessageFrame(ttk.LabelFrame):
    def __init__(self, master, cfg, *args, **kwargs):
        title = "MQTT-Annotate"
        super().__init__(master, *args, text=title, **kwargs)
        self.cfg = cfg

        self.title = StringVar(value="")
        self.time = StringVar(value="") # Nanoseconds since epoch; time.time_ns()

        self.create_widgets()
        self.create_grid()

    def send(self):
        # Get content from input fields
        title = self.title.get()
        desc = self.description.get("1.0", "end")
        time = self.time.get() or None
        if time is not None:
            time = int(time)

        LOG.info('Connecting to MQTT broker ...')
        mqtt_client = mqtt.Client(clean_session=True)
        mqtt_client.enable_logger(LOG)
        mqtt_client.connect(
            host=self.cfg["Service:MQTT:IP"],
            port=self.cfg["Service:MQTT:Port"]
        )
        mqtt_client.loop_start()
        try:
            # MQTT topic & payload and publish
            msg = MQTTMessage(self.cfg["Service:MQTT:TopicBase"], dict(
                title=title,
                description=desc,
            ), time_=time)
            LOG.info("Topic: %r, Payload:\n%s", msg.topic, msg.payload)
            res = mqtt_client.publish(msg.topic, msg.payload, qos=1)
            for _ in range(10):
                if res.is_published():
                    # Clear input fields after publish
                    self.title.set("")
                    self.description.delete("1.0", "end")
                    self.time.set("")
                    break
                sleep(0.1)
        finally:
            mqtt_client.disconnect()
            mqtt_client.loop_stop()

    def create_widgets(self):
        self.lb_title = Label(self, text="title")
        self.en_title = ttk.Entry(self, textvariable=self.title, width=40)

        self.lb_description = Label(self, text="description")
        self.description = Text(self, width=40, height=10, wrap="word")

        self.lb_time = Label(self, text="time")
        self.en_time = ttk.Entry(self, textvariable=self.time, width=40)

        self.bt_send = ttk.Button(self, text='Send', command=self.send)

    def create_grid(self):

        self.lb_title.grid(column=0, row=0, sticky="W")
        self.en_title.grid(column=1, row=0, sticky="W")

        self.lb_description.grid(column=0, row=1, sticky="W")
        self.description.grid(column=1, row=1, sticky="W")

        self.lb_time.grid(column=0, row=2, sticky="W")
        self.en_time.grid(column=1, row=2, sticky="W")

        self.bt_send.grid(columnspan=2, row=3, sticky="WE")


def main(args, cfg):
    root = Tk()
    root.option_add("*tearOff", FALSE)
    root.title(__package__)
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)

    fr_content = ttk.Frame(root, padding=1)
    fr_msg = MessageFrame(fr_content, cfg)

    _common = dict(sticky="NEWS", padx=1, pady=1)
    fr_content.grid(column=0, row=0, sticky="NSEW", padx=10, pady=10)
    fr_msg.grid(column=0, row=0, **_common)

    root.mainloop()
