#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2022 Freemelt AB <opensource@freemelt.com>
#
# SPDX-License-Identifier: GPL-3.0-only

"""This module is the entry point of the mqttannotate"""

# Built-in
import argparse
import logging

# PyPI
import paho.mqtt.client as mqtt

# Package
from mqttannotate import client

# Freemelt
from servicelib import Configurator


def main():
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    cfg = Configurator.load_config("mqttannotate.yaml")
    logging.basicConfig(level=logging.DEBUG)
    try:
        client.main(args, cfg)
    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    main()
